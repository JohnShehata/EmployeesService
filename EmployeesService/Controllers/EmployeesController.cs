﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployeeDataLib.ViewModel;
namespace EmployeesService.Controllers
{
    public class EmployeesController : ApiController
    {

        public HttpResponseMessage Get()
        {
            try
            {
                using (var Context = new EmployeeDataLib.EmployeesEntities())
                {
                    var Employees = Context.Employees.ToList().Select(E => new Employee()
                    {
                        FirstName = E.FirstName,
                        LastName = E.LastName,
                        Birthdate = (E.Birthdate.HasValue ? ((DateTime)E.Birthdate).ToString("dd-MMM-yyyy") : ""),
                        Hiredate = (E.Hiringdate.HasValue ? ((DateTime)E.Hiringdate).ToString("dd-MMM-yyyy") : ""),
                        City = E.City,
                        Address = E.Address,
                        Phone = E.Phone
                    });

                    if (Employees != null && Employees.Count() > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, Employees);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Employees to return");
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

        }

        public HttpResponseMessage Post(EmployeeDataLib.Employee Employee)
        {
            try
            {
                using (var Context=new EmployeeDataLib.EmployeesEntities())
                {
                    Context.Employees.Add(Employee);
                    Context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Created,Employee);
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error occured while inserting the record");
            }
        }

        public HttpResponseMessage PUT(int Id,EmployeeDataLib.Employee Employee)
        {
            try
            {
                using (var Context = new EmployeeDataLib.EmployeesEntities())
                {
                    var Entity = Context.Employees.Where(E => E.ID == Id).FirstOrDefault();
                    if(Entity!=null)
                    {
                        Entity.FirstName = Employee.FirstName;
                        Entity.LastName = Employee.LastName;
                        Entity.Address = Employee.Address;
                        Entity.Birthdate = Employee.Birthdate;
                        Entity.Hiringdate = Employee.Hiringdate;
                        Entity.Phone = Employee.Phone;
                        Entity.Address = Employee.Address;
                        Context.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "record not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "error occured while");
            }
        }
    }
}
