﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDataLib.ViewModel
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Birthdate { get; set; }
        public string Hiredate { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

    }
}
